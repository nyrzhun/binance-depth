module binance

go 1.15

require (
	github.com/adshao/go-binance/v2 v2.3.1
	github.com/bitly/go-simplejson v0.5.0
	github.com/gorilla/websocket v1.4.1
)
