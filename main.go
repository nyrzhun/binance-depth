package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/bitly/go-simplejson"
	"github.com/gorilla/websocket"
)

var addr = flag.String("addr", ":8080", "http service address")

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

const (
	writeWait = 10 * time.Second

	pongWait = 60 * time.Second

	pingPeriod = (pongWait * 9) / 10

	maxMessageSize = 512

	maxDepthLevel = 15

	binanceWSS = "wss://stream.binance.com:9443/ws"

	depthLevel = "depth20"

	frequency = "1000ms"

	websocketTimeout = time.Second * 60
)

type PriceLevel struct {
	Price    string `json:"price"`
	Quantity string `json:"quantity"`
}

type PartialDepth struct {
	LastUpdateID int64        `json:"u"`
	Bids         []PriceLevel `json:"b"`
	Asks         []PriceLevel `json:"a"`
}

func keepAlive(c *websocket.Conn, timeout time.Duration) {
	ticker := time.NewTicker(timeout)

	lastResponse := time.Now()
	c.SetPongHandler(func(msg string) error {
		lastResponse = time.Now()
		return nil
	})

	go func() {
		defer ticker.Stop()
		for {
			deadline := time.Now().Add(10 * time.Second)
			err := c.WriteControl(websocket.PingMessage, []byte{}, deadline)
			if err != nil {
				c.Close()
				return
			}
			<-ticker.C
			if time.Since(lastResponse) > timeout {
				c.Close()
				return
			}
		}
	}()
}

func newJSON(data []byte) (j *simplejson.Json, err error) {
	j, err = simplejson.NewJson(data)
	if err != nil {
		return nil, err
	}
	return j, nil
}

func unmarshalPartialDepthMessage(b []byte) ([]byte, error) {
	j, err := newJSON(b)
	if err != nil {
		return nil, err
	}
	depth := new(PartialDepth)

	depth.LastUpdateID = j.Get("lastupdateId").MustInt64()
	bidsLen := len(j.Get("bids").MustArray())
	if bidsLen > maxDepthLevel {
		bidsLen = maxDepthLevel
	}
	depth.Bids = make([]PriceLevel, bidsLen)
	for i := 0; i < bidsLen; i++ {
		item := j.Get("bids").GetIndex(i)
		depth.Bids[i] = PriceLevel{
			Price:    item.GetIndex(0).MustString(),
			Quantity: item.GetIndex(1).MustString(),
		}
	}
	asksLen := len(j.Get("asks").MustArray())
	if asksLen > maxDepthLevel {
		asksLen = maxDepthLevel
	}
	depth.Asks = make([]PriceLevel, asksLen)
	for i := 0; i < asksLen; i++ {
		item := j.Get("asks").GetIndex(i)
		depth.Asks[i] = PriceLevel{
			Price:    item.GetIndex(0).MustString(),
			Quantity: item.GetIndex(1).MustString(),
		}
	}

	return json.Marshal(depth)
}

func wsPartialBookDepth(ctx context.Context, endpoint string, out chan<- []byte) {
	c, _, err := websocket.DefaultDialer.Dial(endpoint, nil)

	if err != nil {
		log.Println(err)
		close(out)
		return
	}

	defer func() {
		c.Close()
		close(out)
	}()

	keepAlive(c, websocketTimeout)

	var r []byte
	for {
		select {
		case <-ctx.Done():
			return
		default:
			_, message, err := c.ReadMessage()
			if err != nil {
				log.Println(err)
				return
			}
			if r, err = unmarshalPartialDepthMessage(message); err != nil {
				log.Println(err)
				continue
			}
			out <- r
		}
	}
}

func serveHome(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "Not found", http.StatusNotFound)
		return
	}
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, "index.html")
}

func serveWs(w http.ResponseWriter, r *http.Request) {
	// XXX: for testing
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())

	ticker := time.NewTicker(pingPeriod)

	defer func() {
		ticker.Stop()
		conn.Close()
		cancel()
	}()

	_, message, err := conn.ReadMessage()
	if err != nil {
		return
	}
	if string(message) == "undefined" {
		return
	}

	endpoint := fmt.Sprintf("%s/%s@%s@%s", binanceWSS, message, depthLevel,
		frequency)
	send := make(chan []byte, 256)

	go wsPartialBookDepth(ctx, endpoint, send)

	for {
		select {
		case message, ok := <-send:
			conn.SetWriteDeadline(time.Now().Add(writeWait))
			if !ok {
				conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}
			w.Write(message)

			n := len(send)
			for i := 0; i < n; i++ {
				w.Write(<-send)
			}

			if err := w.Close(); err != nil {
				return
			}
		case <-ticker.C:
			conn.SetWriteDeadline(time.Now().Add(writeWait))
			if err := conn.WriteMessage(websocket.PingMessage, nil); err != nil {
				return
			}
		case <-ctx.Done():
			return
		}
	}
}

func main() {
	http.Handle("/", http.FileServer(http.Dir("./static")))
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(w, r)
	})
	err := http.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
