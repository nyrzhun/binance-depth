;(() => {
  let connGlobal = false
  var tableBids = new DataTable('#bidsID', {
      "autowidth": true,
      "iDisplayLength": 16,
      "searching": false,
      "ordering": false,
      "info": false,
      "lengthChange": false,
      "bPaginate": false,
  });
  var tableAsks = new DataTable('#asksID', {
      "autowidth": true,
      "iDisplayLength": 16,
      "searching": false,
      "ordering": false,
      "info": false,
      "lengthChange": false,
      "bPaginate": false,
  });
  function dial(text) {
	try {
		if (connGlobal.readyState === WebSocket.OPEN || connGlobal.readyState === WebSocket.CONNECTING) {
			connGlobal.close(1000, "Conn done")
		}
	} catch (err) {
		console.log(err)
	}

    var conn = new WebSocket(`ws://localhost:8080/ws`)

    conn.addEventListener("open", ev => {
	  connGlobal = conn
      conn.send(text)
      console.log("websocket connected")
    })

    conn.addEventListener("message", ev => {
      if (typeof ev.data !== "string") {
        console.error("unexpected message type", typeof ev.data)
        return
      }
      redrawTables(ev.data)
    })
  }

  const subscribeForm = document.getElementById("subscribe-form")
  const messageInput = document.getElementById("message-input")

  function redrawTables(data, error) {
    const p = document.createElement("p")
    try {
        let msgs = JSON.parse(data);
		let bids = msgs.b
        let asks = msgs.a

        if (Array.isArray(bids)) {
			tableBids.clear();
            var bidsSum = 0
            for (let bid of bids) {
                tableBids.row.add([bid.price, bid.quantity])
                bidsSum = bidsSum + parseFloat(bid.quantity)
            }
            tableBids.row.add(['Total', bidsSum.toFixed(9)])
			tableBids.draw();
        }

        if (Array.isArray(asks)) {
			tableAsks.clear();
            var asksSum = 0
            for (let ask of asks) {
                tableAsks.row.add([ask.price, ask.quantity])
                asksSum = asksSum + parseFloat(ask.quantity)
            }
            tableAsks.row.add(['Total', asksSum.toFixed(9)])
			tableAsks.draw();
        }
    } catch (e) {
        console.log('redrawTables: ', e);
    }  
  }

  subscribeForm.onsubmit = async ev => {
    ev.preventDefault()

    const msg = messageInput.value
    if (msg === "") {
      return
    }
    messageInput.value = ""
	document.getElementById("depth-paragraph").innerHTML = msg;
    try {
        dial(msg)
    } catch (err) {
		console.log(err)
    }
  }
})()
